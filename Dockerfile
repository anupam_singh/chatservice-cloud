FROM schoolofdevops/voteapp-mvn:v0.1.0

# Set the location of the verticles
ENV VERTICLE_HOME /code
WORKDIR $VERTICLE_HOME

ADD pom.xml $VERTICLE_HOME/pom.xml
RUN mvn dependency:resolve
RUN mvn verify

# Adding source, compile and package into a fat jar
ADD src/main $VERTICLE_HOME/src/main
RUN mvn package

EXPOSE 8090
CMD java -jar target/chatservice-fat.jar
