package com.stackOverFlow;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.ServerWebSocket;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.SessionHandler;
import io.vertx.ext.web.sstore.LocalSessionStore;
import io.vertx.rabbitmq.RabbitMQClient;
import io.vertx.rabbitmq.impl.RabbitMQClientImpl;
import io.vertx.redis.RedisClient;
import io.vertx.redis.RedisOptions;

public class ChatService extends AbstractVerticle{
	
	@Override
	public void start(Future<Void> startFuture) throws Exception {
		super.start(startFuture);
		System.out.println("ChatService.start() Anupammmm");
//		RabbitMQClient rabbit = new RabbitMQClientImpl(vertx, new JsonObject().put("uri", "amqp://35.190.147.51:5672"));
//		RabbitMQClient rabbit = new RabbitMQClientImpl(vertx, new JsonObject().put("uri", "amqp://10.197.54.23:8090"));
		RabbitMQClient rabbit = new RabbitMQClientImpl(vertx, new JsonObject().put("uri", "amqp://104.196.128.2:8090"));
		rabbit.start(response -> {
			System.out.println("ChatService.start(): "+response.succeeded());
		});
		
		RedisOptions config = new RedisOptions().setHost("35.188.227.50");
//		RedisOptions config = new RedisOptions().setHost("10.197.54.23:8030");
		RedisClient redis = RedisClient.create(vertx, config);
		
		redis.set("name", "anupam", response-> {
			System.out.println("ChatService.start() RedisClient PUT succeeded"+response.succeeded());
		});
		
		redis.get("name", response-> {
			System.out.println("ChatService.start() RedisClient GET succeeded"+response.succeeded());
			System.out.println("ChatService.start() response.result(): "+response.result());
		});
		redis.clientList(handler -> {
			System.out.println("ChatService.start() clientList: "+handler.succeeded());
			System.out.println("ChatService.start() clientList: "+handler.result());
		});
		
		final EventBus eventBus = vertx.eventBus();	
		final Pattern chatUrlPattern = Pattern.compile("/chat/(\\w+)");
		Router router = Router.router(vertx);
		// body handler
		router.route().handler(BodyHandler.create());
		
//		router.get("/").handler(rctx -> {
//			rctx.response().setStatusCode(200).putHeader("Content-Type", "text/html")
//					.sendFile("webroot/chat.html");
//		});
		
//		router.get(".*\\.(css|js)$").handler(rctx -> {
//			rctx.response().sendFile("web/" + new File(rctx.request().path()));
//		});
		
		// static content
//	    router.route("/*").handler(StaticHandler.create());
		router.route().handler(SessionHandler.create(LocalSessionStore.create(vertx)));
		
//		vertx.createHttpServer().requestHandler(router::accept).listen(8070);
		
		vertx.createHttpServer().websocketHandler(new Handler<ServerWebSocket>() {

			@Override
			public void handle(ServerWebSocket ws) {
				System.out.println("ChatService.start(...).new Handler() {...}.handle() ws.path(): "+ws.path());
				final Matcher m = chatUrlPattern.matcher(ws.path());
				System.out.println("WebserverVerticleSingleInstance.start(...).new Handler() {...}.handle() m.matches(): "+m.matches());
				if (!m.matches()) {
					ws.reject();
					return;
				}
				
				final String chatRoom = m.group(1);
				final String id = ws.textHandlerID();
				System.out.println("registering new connection with id: " + id + " for chat-room: " + chatRoom);
				
				LocalMap<String, String> localMap = vertx.sharedData().getLocalMap("chat.room." + chatRoom);
				localMap.put(id, chatRoom);
				
				ws.closeHandler(new Handler<Void>() {
					@Override
					public void handle(final Void event) {
						System.out.println("un-registering connection with id: " + id + " from chat-room: " + chatRoom);
						LocalMap<String, String> localMap = vertx.sharedData().getLocalMap("chat.room." + chatRoom);
						localMap.remove(id);
					}
				});
				
				ws.handler(new Handler<Buffer>() {

					@Override
					public void handle(Buffer data) {
						ObjectMapper m = new ObjectMapper();
						try {
							JsonNode rootNode = m.readTree(data.toString());
							((ObjectNode) rootNode).put("received", new Date().toString());
							String jsonOutput = m.writeValueAsString(rootNode);
							System.out.println("json generated: " + jsonOutput + " data.toString():   "+data.toString() + " chatRoom: "+chatRoom);
							LocalMap<String, String> localMap = vertx.sharedData().getLocalMap("chat.room." + chatRoom);
							System.out.println(
									"ChatService.start(...).new Handler() {...}.handle(...).new Handler() {...}.handle() localMap size: "+localMap.size());
							Set<Entry<String, String>> set = localMap.entrySet();
							Iterator<Entry<String, String>> iter = set.iterator();
							JsonArray wsAlreadyNotifiedArray = new JsonArray();
							while (iter.hasNext()) {
								Map.Entry<String, String> entry = (Map.Entry<String, String>) iter
										.next();
								String chatterId = entry.getKey();
								wsAlreadyNotifiedArray.add(chatterId);
								eventBus.send(chatterId, jsonOutput);
							}
							System.out.println(
									"ChatService..handle() isConnected: "+rabbit.isConnected() + "  isOpenChannel: "+rabbit.isOpenChannel());
							if(rabbit.isConnected() && rabbit.isOpenChannel()) {
								publishMessage(rabbit,jsonOutput,wsAlreadyNotifiedArray,chatRoom);
							} else {
								rabbit.start(response -> {
									System.out.println("ChatService.start() get RESPONSE ??????????");
									publishMessage(rabbit,jsonOutput,wsAlreadyNotifiedArray,chatRoom);
								});
							}
							
						} catch (IOException e) {
							ws.reject();
						}
					}
					
				});
				
			}
			
		}).listen(8030);
		
		vertx.eventBus().consumer("handle.chatMesage", msg -> {
			JsonObject json = (JsonObject) msg.body();
			System.out.println("Inside ChatService handle.chatMesage Got message: " + json.encodePrettily());
			JsonObject body = json.getJsonObject("body");
			String chatRoom = body.getString("chatRoom");
			System.out.println("ChatService.start() handle.chatMesage chatRoom: "+chatRoom);
			LocalMap<String, String> localMap = vertx.sharedData().getLocalMap("chat.room." + chatRoom);
			System.out.println("ChatService.start() localMap size: "+localMap.size());
			Set<Entry<String, String>> set = localMap.entrySet();
			Iterator<Entry<String, String>> iter = set.iterator();
			JsonArray wsAlreadyNotifiedArray = body.getJsonArray("wsAlreadyNotified");
			while (iter.hasNext()) {
				Map.Entry<String, String> entry = (Map.Entry<String, String>) iter
						.next();
				String chatterId = entry.getKey();
				System.out.println("ChatService.start() handle.chatMesage chatterId: "+chatterId);
				System.out.println("ChatService.start() handle.chatMesage wsAlreadyNotifiedArray.contains(chatterId): "+wsAlreadyNotifiedArray.contains(chatterId));
				if(!wsAlreadyNotifiedArray.contains(chatterId)) {
//					sending messages to those scokets which are not the part of already sent list
					eventBus.send(chatterId, body.getString("message"));
				}
			}
		});
		System.out.println("Inside ChatService handle.chatMesage isConnected: " + rabbit.isConnected() + " rabbit.isOpenChannel(): "+rabbit.isOpenChannel());
		if (rabbit.isConnected() && rabbit.isOpenChannel()) {
			consumeMessage(rabbit);
		} else {
			rabbit.start(response -> {
				consumeMessage(rabbit);
			});
		}
		
	}
	
	private void publishMessage(RabbitMQClient rabbit,String jsonOutput,JsonArray wsAlreadyNotifiedArray,String chatRoom) {
		// Setup the link between rabbitmq consumer and event bus address
		JsonObject body = new JsonObject();
		body.put("message", jsonOutput);
		body.put("wsAlreadyNotified", wsAlreadyNotifiedArray);
		body.put("chatRoom", chatRoom);
		JsonObject message = new JsonObject();
		message.put("properties", new JsonObject().put("contentType", "application/json"));
		message.put("body", body);
		System.out.println("ChatService.publishMessage() message: "+message.encodePrettily());
		rabbit.basicPublish("", "chat.message", message, result -> {
			if (result.succeeded()) {
				System.out.println("Message Published!");
			} else {
				System.out.println("Failed");
				result.cause().printStackTrace();
			}
		});
	}
	
	private void consumeMessage(RabbitMQClient rabbit) {
		rabbit.basicConsume("chat.message", "handle.chatMesage", result -> {
			if (result.succeeded()) {
				System.out.println("ChatService.consumeMessage()");
				System.out.println("ChatService.consumeMessage() result.result(): " + result.result());
				System.out.println("Inside ChatService RabbitMQ consumer created !");
			} else {
				System.out.println("Inside ChatService RabbitMQ consumer creation Failed");
				result.cause().printStackTrace();
			}
		});
	}

}
