package com.stackOverFlow;

import java.io.IOException;
import java.time.Instant;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.ServerWebSocket;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.SessionHandler;
import io.vertx.ext.web.sstore.LocalSessionStore;
import io.vertx.rabbitmq.RabbitMQClient;
import io.vertx.rabbitmq.impl.RabbitMQClientImpl;
import io.vertx.redis.RedisClient;
import io.vertx.redis.RedisOptions;

public class ChatService_2 extends AbstractVerticle{
	
	@Override
	public void start(Future<Void> startFuture) throws Exception {
		super.start(startFuture);
		System.out.println("ChatService_2.start()");
//		RabbitMQClient rabbit = new RabbitMQClientImpl(vertx, new JsonObject().put("uri", "amqp://35.190.147.51:5672"));
//		RabbitMQClient rabbit = new RabbitMQClientImpl(vertx, new JsonObject().put("uri", "amqp://10.197.54.23:8090"));
		RabbitMQClient rabbit = new RabbitMQClientImpl(vertx, new JsonObject().put("uri", "amqp://104.196.128.2:8090"));
		rabbit.start(response -> {
			System.out.println("ChatService.start(): "+response.succeeded());
		});
		
		RedisOptions config = new RedisOptions().setHost("35.188.227.50");
//		RedisOptions config = new RedisOptions().setHost("10.197.54.23:8030");
		RedisClient redis = RedisClient.create(vertx, config);
		
		final EventBus eventBus = vertx.eventBus();	
		final Pattern chatUrlPattern = Pattern.compile("/chat/(\\w+)");
		Router router = Router.router(vertx);
		// body handler
		router.route().handler(BodyHandler.create());
		router.route().handler(CorsHandler.create("*")
			      .allowedMethod(HttpMethod.GET)
			      .allowedMethod(HttpMethod.POST)
			      .allowedMethod(HttpMethod.PUT)
			      .allowedMethod(HttpMethod.DELETE)
			      .allowedMethod(HttpMethod.OPTIONS)
			      .allowCredentials(true)
			      .allowedHeader("X-PINGARUNER")
			      .allowedHeader("Access-Control-Allow-Method")
			      .allowedHeader("Access-Control-Allow-Origin")
			      .allowedHeader("Access-Control-Allow-Credentials")
			      .allowedHeader("Authorization")
			      .allowedHeader("Content-Type"));
		
//		router.get("/").handler(rctx -> {
//			rctx.response().setStatusCode(200).putHeader("Content-Type", "text/html")
//					.sendFile("webroot/chat.html");
//		});
		
//		router.get(".*\\.(css|js)$").handler(rctx -> {
//			rctx.response().sendFile("web/" + new File(rctx.request().path()));
//		});
		
		// static content
//	    router.route("/*").handler(StaticHandler.create());
		router.route().handler(SessionHandler.create(LocalSessionStore.create(vertx)));
		
//		vertx.createHttpServer().requestHandler(router::accept).listen(8070);
		
		vertx.createHttpServer().websocketHandler(new Handler<ServerWebSocket>() {

			@Override
			public void handle(ServerWebSocket ws) {
				System.out.println("ChatService_2.start(...).new Handler() {...}.handle() ws.path(): "+ws.path());
				final Matcher m = chatUrlPattern.matcher(ws.path());
				System.out.println("WebserverVerticleSingleInstance.start(...).new Handler() {...}.handle() Anupam m.matches(): "+m.matches());
				if (!m.matches()) {
					ws.reject();
					return;
				}
				
				final String loggedin_user = m.group(1);
				StringTokenizer tokenizer = new StringTokenizer(loggedin_user,":");
				int count = 0;
				String[] strArray = new String[tokenizer.countTokens()];
				while(tokenizer.hasMoreTokens()) {
					strArray[count] = tokenizer.nextToken();
					count ++;
				}
//				final String loggedin_user_id = m.group(1);
				final String loggedin_user_id = strArray[0];
				final String loggedin_user_name = strArray[0];
				final String id = ws.textHandlerID();
				System.out.println("registering new connection with id: " + id + " for user id: " + loggedin_user_id);
				
//				this local map contains all the web socket users in this chat instance
				LocalMap<String, String> localMap_all_users = vertx.sharedData().getLocalMap("all.users");
			 	Set<Entry<String, String>> all_users_set = localMap_all_users.entrySet();
			 	Iterator<Entry<String, String>> all_users_iter = all_users_set.iterator();
			 	while (all_users_iter.hasNext()) {
					Map.Entry<java.lang.String, java.lang.String> user_entry = (Map.Entry<java.lang.String, java.lang.String>) all_users_iter
							.next();
					String socketId = user_entry.getValue();
					JsonObject status_msg = new JsonObject();
					status_msg.put("type", "status");
					status_msg.put("sender_id", loggedin_user_id);
					status_msg.put("status", "online");
					eventBus.send(socketId, status_msg.encodePrettily());
				}
				
				LocalMap<String, String> localMap = vertx.sharedData().getLocalMap("user.id." + loggedin_user_id);
				
				redis.sismember("chat_users_set", loggedin_user_id, handler -> {
					System.out.println("ChatService_2.start IS USERINFO A MEMBER IN REDIS for ONLINE succeeded "+handler.succeeded());
					if(handler.succeeded()) {
						System.out.println("ChatService_2.start IS USERINFO A MEMBER IN REDIS for ONLINE result "+handler.result());
						if(handler.result() == 0) {
							redis.sadd("chat_users_set", loggedin_user_id, handler_2 -> {
								System.out.println("ChatService_2.start SADD USERINFO TO REDIS for ONLINE succeeded "+handler_2.succeeded());
								if(handler_2.succeeded()) {
									JsonObject userInfo = new JsonObject();
									userInfo.put("name", loggedin_user_name);
									userInfo.put("status", "online");
									redis.set(loggedin_user_id, userInfo.encodePrettily(), handler_3 -> {
										System.out.println("ChatService_2.start SET USERINFO FROM REDIS for ONLINE succeeded "+handler_3.succeeded());
									});
								}
							});
						} else {
							System.out.println("ChatService_2.start USERINFO ALREADY EXISTS IN SET OF REDIS for ONLINE succeeded so set the new VALUE");
							JsonObject userInfo = new JsonObject();
							userInfo.put("name", loggedin_user_name);
							userInfo.put("status", "online");
							redis.set(loggedin_user_id, userInfo.encodePrettily(), handler_3 -> {
								System.out.println("ChatService_2.start SET USERINFO FROM REDIS for ONLINE succeeded "+handler_3.succeeded());
							});
						}
					}
				});
				
				
//				redis.get("online_users_info", handler -> {
//					System.out.println("ChatService_2.start GET USERINFO FROM REDIS for ONLINE succeeded "+handler.succeeded());
//					if(handler.succeeded()) {
//						String str = handler.result();
//						JsonArray infoArray;
//						if(str != null && str.length() > 0) {
//							infoArray = new JsonArray(str);
//						} else {
//							infoArray = new JsonArray();
//						}
//						JsonObject userInfo = new JsonObject();
//						userInfo.put("name", loggedin_user_name);
//						userInfo.put("status", "online");
//						System.out.println("ChatService_2.start SET USERINFO FROM REDIS for ONLINE userInfo.encodePrettily(): "+userInfo.encodePrettily());
//						infoArray.add(userInfo);
//						redis.set("online_users_info", infoArray.encodePrettily(), handler_2 -> {
//							System.out.println("ChatService_2.start SET USERINFO FROM REDIS for ONLINE succeeded "+handler_2.succeeded());
//						});
//					}
//				});
				localMap.put(loggedin_user_id, id);
				localMap_all_users.put(loggedin_user_id, id);
//				JsonObject userInfo = new JsonObject();
//				userInfo.put("name", loggedin_user_name);
//				userInfo.put("status", "online");
//				System.out.println("ChatService_2.start USERINFO SET REDIS for ONLINE userInfo.encodePrettily(): "+userInfo.encodePrettily());
//				redis.set(loggedin_user_id, userInfo.encodePrettily(), handler -> {
//					System.out.println("ChatService_2.start USERINFO SET REDIS for ONLINE succeeded "+handler.succeeded());
//				});
				ws.closeHandler(new Handler<Void>() {
					@Override
					public void handle(final Void event) {
						LocalMap<String, String> localMap = vertx.sharedData().getLocalMap("user.id." + loggedin_user_id);
						
//						this local map contains all the web socket users in this chat instance
						LocalMap<String, String> localMap_all_users = vertx.sharedData().getLocalMap("all.users");
					 	Set<Entry<String, String>> all_users_set = localMap_all_users.entrySet();
					 	Iterator<Entry<String, String>> all_users_iter = all_users_set.iterator();
					 	while (all_users_iter.hasNext()) {
							Map.Entry<java.lang.String, java.lang.String> user_entry = (Map.Entry<java.lang.String, java.lang.String>) all_users_iter
									.next();
							String socketId = user_entry.getValue();
							JsonObject status_msg = new JsonObject();
							status_msg.put("type", "status");
							status_msg.put("sender_id", loggedin_user_id);
							status_msg.put("status", "offline");
							eventBus.send(socketId, status_msg.encodePrettily());
						}
//						redis.get("online_users_info", handler -> {
//							System.out.println("ChatService_2.start GET USERINFO FROM REDIS for OFFLINE succeeded "+handler.succeeded());
//							if(handler.succeeded()) {
//								String str = handler.result();
//								JsonArray infoArray;
//								if(str != null && str.length() > 0) {
//									infoArray = new JsonArray(str);
//								} else {
//									infoArray = new JsonArray();
//								}
//								JsonObject userInfo = new JsonObject();
//								userInfo.put("name", loggedin_user_name);
//								userInfo.put("status", "offline");
//								userInfo.put("lastactive", new Date().toString());
//								System.out.println("ChatService_2.start SET USERINFO FROM REDIS for OFFLINE userInfo.encodePrettily(): "+userInfo.encodePrettily());
//								infoArray.add(userInfo);
//								redis.set("online_users_info", infoArray.encodePrettily(), handler_2 -> {
//									System.out.println("ChatService_2.start SET USERINFO FROM REDIS for OFFLINE succeeded "+handler_2.succeeded());
//								});
//							}
//						});
						
						JsonObject userInfo = new JsonObject();
						userInfo.put("name", loggedin_user_name);
						userInfo.put("status", "offline");
						userInfo.put("lastactive", Instant.now());
						Instant lastactive = userInfo.getInstant("lastactive");
						System.out.println(
								"ChatService_2.start(...).new Handler() {...}.handle(...).new Handler() {...}.handle() lastactive: "+lastactive);
						System.out.println("ChatService_2.start SET USERINFO FROM REDIS for OFFLINE userInfo.encodePrettily(): "+userInfo.encodePrettily());
						redis.set(loggedin_user_id, userInfo.encodePrettily(), handler_2 -> {
							System.out.println("ChatService_2.start SET USERINFO FROM REDIS for OFFLINE succeeded "+handler_2.succeeded());
						});
						
						localMap.remove(loggedin_user_id);
						localMap_all_users.remove(loggedin_user_id);
//						JsonObject userInfo = new JsonObject();
//						userInfo.put("name", loggedin_user_name);
//						userInfo.put("status", "offline");
//						userInfo.put("lastactive", new Date().toString());
//						localMap.put(loggedin_user_id, id);
//						System.out.println("ChatService_2.start USERINFO SET REDIS for OFFLINE userInfo.encodePrettily(): "+userInfo.encodePrettily());
//						redis.set(loggedin_user_id, userInfo.encodePrettily(), handler -> {
//							System.out.println("ChatService_2.start USERINFO SET REDIS for OFFLINE succeeded "+handler.succeeded());
//						});
					}
				});
				
				ws.handler(new Handler<Buffer>() {

					@Override
					public void handle(Buffer data) {
						ObjectMapper m = new ObjectMapper();
						System.out.println(
								"ChatService_2.start(...).new Handler() {...}.handle(...).new Handler() {...}.handle() data.toString(): "+data.toString());
						try {
							JsonNode rootNode = m.readTree(data.toString());
							((ObjectNode) rootNode).put("received", new Date().toString());
							String jsonOutput = m.writeValueAsString(rootNode);
							JsonObject jsonOutputObj = new JsonObject(jsonOutput);
							System.out.println("json generated: " + jsonOutput + " data.toString():   "+data.toString() + " loggedin_user_id: "+loggedin_user_id);
//							LocalMap<String, String> localMap = vertx.sharedData().getLocalMap("user.id." + loggedin_user_id);
//							System.out.println(
//									"ChatService.start(...).new Handler() {...}.handle(...).new Handler() {...}.handle() localMap size: "+localMap.size());
//							Set<Entry<String, String>> set = localMap.entrySet();
//							Iterator<Entry<String, String>> iter = set.iterator();
//							JsonArray wsAlreadyNotifiedArray = new JsonArray();
//							while (iter.hasNext()) {
//								Map.Entry<String, String> entry = (Map.Entry<String, String>) iter
//										.next();
//								String chatterId = entry.getValue();
//								wsAlreadyNotifiedArray.add(chatterId);
//								JsonObject message = new JsonObject();
//								message.put("reciever_id", jsonOutputObj.getString("reciever_id"));
//								message.put("sender_id", loggedin_user_id);
//								message.put("received_time", new Date().toString());
//								message.put("message", jsonOutputObj.getString("message"));
////								eventBus.send(chatterId, jsonOutput);
//								System.out.println(
//										"ChatService_2.start(...).new Handler() {...}.handle(...).new Handler() {...}.handle() message.encodePrettily(): "+message.encodePrettily());
//								eventBus.send(chatterId, message.encodePrettily());
////								eventBus.send(chatterId, jsonOutput);
//							}
							JsonArray wsAlreadyNotifiedArray = new JsonArray();
							sendMessage(eventBus,loggedin_user_id,jsonOutputObj,loggedin_user_id,wsAlreadyNotifiedArray);
							sendMessage(eventBus,loggedin_user_id,jsonOutputObj,jsonOutputObj.getString("reciever_id"),wsAlreadyNotifiedArray);
							
							System.out.println(
									"ChatService..handle() isConnected: "+rabbit.isConnected() + "  isOpenChannel: "+rabbit.isOpenChannel());
							if(rabbit.isConnected() && rabbit.isOpenChannel()) {
//								publishMessage(rabbit,jsonOutput,wsAlreadyNotifiedArray,jsonOutputObj.getString("reciever_id"),loggedin_user_id);
								publishMessage(rabbit,jsonOutput,wsAlreadyNotifiedArray,loggedin_user_id);
							} else {
								rabbit.start(response -> {
									System.out.println("ChatService.start() get RESPONSE ??????????");
//									publishMessage(rabbit,jsonOutput,wsAlreadyNotifiedArray,jsonOutputObj.getString("reciever_id"),loggedin_user_id);
									publishMessage(rabbit,jsonOutput,wsAlreadyNotifiedArray,loggedin_user_id);
								});
							}
							
						} catch (IOException e) {
							System.out.println(
									"ChatService_2.start(...).new Handler() {...}.handle(...).new Handler() {...}.handle() e.getMessage(): "+e.getMessage());
							e.printStackTrace();
							ws.reject();
						}
					}
					
				});
				
			}
			
		}).listen(8090);
		
		vertx.eventBus().consumer("handle.chatMesage", msg -> {
			JsonObject json = (JsonObject) msg.body();
			System.out.println("Inside ChatService handle.chatMesage Got message: " + json.encodePrettily());
//			JsonObject body = json.getJsonObject("body");
//			System.out.println("ChatService_2.start() body.encodePrettily: "+body.encodePrettily());
//			String reciever_id = body.getString("reciever_id");
//			System.out.println("ChatService.start() handle.chatMesage reciever_id: "+reciever_id);
//			System.out.println("ChatService_2.start() sender_id: "+body.getString("sender_id"));
//			LocalMap<String, String> localMap = vertx.sharedData().getLocalMap("user.id." + reciever_id);
//			System.out.println("ChatService.start() localMap size: "+localMap.size());
//			Set<Entry<String, String>> set = localMap.entrySet();
//			Iterator<Entry<String, String>> iter = set.iterator();
//			JsonArray wsAlreadyNotifiedArray = body.getJsonArray("wsAlreadyNotified");
//			while (iter.hasNext()) {
//				Map.Entry<String, String> entry = (Map.Entry<String, String>) iter
//						.next();
//				String chatterId = entry.getValue();
//				System.out.println("ChatService.start() handle.chatMesage chatterId: "+chatterId);
//				System.out.println("ChatService.start() handle.chatMesage wsAlreadyNotifiedArray.contains(chatterId): "+wsAlreadyNotifiedArray.contains(chatterId));
//				if(!wsAlreadyNotifiedArray.contains(chatterId)) {
////					sending messages to those scokets which are not the part of already sent list
//					JsonObject jsonOutputObj = new JsonObject(body.getString("message"));
//					JsonObject message = new JsonObject();
//					message.put("message", jsonOutputObj.getString("message"));
//					
//					message.put("reciever_id", jsonOutputObj.getString("reciever_id"));
//					message.put("sender_id", body.getString("sender_id"));
//					message.put("message", jsonOutputObj.getString("message"));
//					message.put("received_time", new Date().toString());
////					eventBus.send(chatterId, message);
//					System.out.println("ChatService_2.start() message.encodePrettily(): "+message.encodePrettily());
//					eventBus.send(chatterId, message.encodePrettily());
////					eventBus.send(chatterId, body.getString("message"));
//				}
//			}
		});
		System.out.println("Inside ChatService handle.chatMesage isConnected: " + rabbit.isConnected() + " rabbit.isOpenChannel(): "+rabbit.isOpenChannel());
		if (rabbit.isConnected() && rabbit.isOpenChannel()) {
			consumeMessage(rabbit);
		} else {
			rabbit.start(response -> {
				consumeMessage(rabbit);
			});
		}
		
	}
	
//	private void publishMessage(RabbitMQClient rabbit,String jsonOutput,JsonArray wsAlreadyNotifiedArray,String reciever_id,String sender_id) {
//		// Setup the link between rabbitmq consumer and event bus address
//		JsonObject body = new JsonObject();
//		body.put("message", jsonOutput);
//		body.put("wsAlreadyNotified", wsAlreadyNotifiedArray);
//		body.put("reciever_id", reciever_id);
//		body.put("sender_id", sender_id);
//		System.out.println("ChatService_2.publishMessage() body.encodePrettily(): "+body.encodePrettily());
//		JsonObject message = new JsonObject();
//		message.put("properties", new JsonObject().put("contentType", "application/json"));
//		message.put("body", body);
//		System.out.println("ChatService.publishMessage() message: "+message.encodePrettily());
//		rabbit.basicPublish("", "chat.message", message, result -> {
//			if (result.succeeded()) {
//				System.out.println("Message Published!");
//			} else {
//				System.out.println("Failed");
//				result.cause().printStackTrace();
//			}
//		});
//	}
	
	private void publishMessage(RabbitMQClient rabbit,String jsonOutput,JsonArray wsAlreadyNotifiedArray,String chatRoom) {
		System.out.println("ChatService_2.publishMessage() NEWWWWWWWWWWWWW");
		// Setup the link between rabbitmq consumer and event bus address
		JsonObject body = new JsonObject();
		body.put("message", jsonOutput);
		body.put("wsAlreadyNotified", wsAlreadyNotifiedArray);
		body.put("chatRoom", chatRoom);
		JsonObject message = new JsonObject();
		message.put("properties", new JsonObject().put("contentType", "application/json"));
		message.put("body", body);
		System.out.println("ChatService.publishMessage() message: "+message.encodePrettily());
		rabbit.basicPublish("", "chat.message", message, result -> {
			if (result.succeeded()) {
				System.out.println("Message Publisheddddd!");
			} else {
				System.out.println("Failed");
				result.cause().printStackTrace();
			}
		});
	}
	
	private void consumeMessage(RabbitMQClient rabbit) {
		rabbit.basicConsume("chat.message", "handle.chatMesage", result -> {
			if (result.succeeded()) {
				System.out.println("ChatService.consumeMessage()");
				System.out.println("ChatService.consumeMessage() result.result(): " + result.result());
				System.out.println("Inside ChatService RabbitMQ consumer created !");
			} else {
				System.out.println("Inside ChatService RabbitMQ consumer creation Failed");
				result.cause().printStackTrace();
			}
		});
	}
	
	private void sendMessage(final EventBus eventBus, final String loggedin_user_id,final JsonObject jsonOutputObj,final String user_id,final JsonArray wsAlreadyNotifiedArray) {
		System.out.println("ChatService_2.sendMessage()>>>>>>>>>>>>>>>>>");
		LocalMap<String, String> localMap = vertx.sharedData().getLocalMap("user.id." + user_id);
		System.out.println(
				"ChatService.start(...).new Handler() {...}.handle(...).new Handler() {...}.handle() localMap size: "+localMap.size());
		Set<Entry<String, String>> set = localMap.entrySet();
		Iterator<Entry<String, String>> iter = set.iterator();
		while (iter.hasNext()) {
			Map.Entry<String, String> entry = (Map.Entry<String, String>) iter
					.next();
			String chatterId = entry.getValue();
			wsAlreadyNotifiedArray.add(chatterId);
			JsonObject message = new JsonObject();
			message.put("type", "message");
			message.put("reciever_id", jsonOutputObj.getString("reciever_id"));
			message.put("sender_id", loggedin_user_id);
			message.put("received_time", new Date().toString());
			message.put("message", jsonOutputObj.getString("message"));
//			eventBus.send(chatterId, jsonOutput);
			System.out.println(
					"ChatService_2.start(...).new Handler() {...}.handle(...).new Handler() {...}.handle() message.encodePrettily(): "+message.encodePrettily());
			eventBus.send(chatterId, message.encodePrettily());
//			eventBus.send(chatterId, jsonOutput);
		}
	}

}
